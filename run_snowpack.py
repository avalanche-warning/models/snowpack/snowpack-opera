#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script performs SNOWPACK runs.

from awgeo import get_epsg_code
import awio
from awset.domain_settings import exists, get, getlist, getattribute
from awsmet import SMETParser
from snowpacktools.snowpro import snowpro

from datetime import datetime, timedelta
import glob
import json
import os
from pathlib import Path
import re
import sys
import subprocess

_data_is_young = 3 # hours
_map_generation_script = os.path.expanduser("~opera/dashboard/generate_map.py")

def get_dl_path(domain: str, snow=False):
    """Return place for meteo data or snow files"""
    return os.path.join(os.path.expanduser("~snowpack/opera"), "snow" if snow else "data", domain)

def _download_sp_stations(stations: list, sdate: str, edate: str, domain: str) -> list:
    """Prepare list of stations to run and download data for them."""

    stations_not_updated = []
    data_src = get(["snowpack", "meteo", "source"], domain)
    if data_src == "lwdt":
        download_script_path = os.path.expanduser("~data/query/wiski/wiski_rest.py")
        for station in stations:
            tstart = None
            output_dir = get_dl_path(domain)
            stat_file = os.path.join(get_dl_path(domain), station + ".smet")
            data_span = f"{sdate}/{edate}" # date format of lwdt API

            if os.path.isfile(stat_file):
                if awio.is_new_file(stat_file, hours=_data_is_young): # check file modification date
                    print(f'[i] Skipping data download for station "{station}" (younger than {_data_is_young} hours).')
                    stations_not_updated.append(station)
                    continue # we already have new data
                else:
                    tend = SMETParser.get_timespan(stat_file)[1] # check end of data in file
                    tend = datetime.strptime(tend, "%Y-%m-%dT%H:%M:%SZ")
                    tstart = (tend + timedelta(seconds=1)).isoformat()
                    data_span = f"{tstart}Z/{edate}" # end of old data file is beginning of new query
                    output_dir = os.path.join(output_dir, "tmp")
            subprocess.call(["python3", download_script_path, station, data_span, output_dir])
            if tstart: # we have new data ready to append
                smet_old = SMETParser(stat_file)
                smet_new = SMETParser(os.path.join(get_dl_path(domain), "tmp", station + ".smet"))
                smet_old.append(smet_new, checks=False)
                smet_old.save()
                os.remove(smet_new.smetfile)

        # check if there are smets from older setups which are not needed for runs like current one:
        all_smets = glob.glob(os.path.join(get_dl_path(domain), "*.smet"))
        for smet in all_smets:
            stat = os.path.basename(os.path.splitext(smet)[0])
            if not stat in stations:
                print(f'[i] Removing rogue data file for station "{stat}"')
                os.remove(smet)
        try:
            os.rmdir(os.path.join(get_dl_path(domain), "tmp"))
        except OSError:
            pass
    else:
        print("[E] Module for data source {data_src} not configured.")
        exit(1)

    return stations_not_updated

def _create_aux_files(aspects: str, angles: str, domain: str):
    """Generate meta files needed by SNOWPACK."""
    datafiles = glob.glob(get_dl_path(domain) + "/*.smet")
    snofile_script_path = os.path.expanduser("~snowpack/metadata/sno_generator.py")
    os.makedirs(get_dl_path(domain, snow=True), exist_ok=True)
    [subprocess.call(["python3", snofile_script_path, file, get_dl_path(domain, snow=True), "True", aspects, angles]) for file in datafiles]

def _update_ini(stations: list, station: str, number_slopes: int, flavor: str, preprocessing_flavor: str, domain: str):
    """Update SNOWPACK ini file with info from our domain settings file."""
    ini_template = f"./input/{flavor}.ini.template"
    with open(ini_template, "r") as file:
        data = file.read()
        # use full path so that the INI is usable from other parts of the toolchain:
        data = data.replace("$DOMAIN", domain)
        data = data.replace("$STATIONID", station)
        data = data.replace("$IMPORT_PREPROCESSING", awio.get_scriptpath(__file__) + f"/input/preprocessing_{preprocessing_flavor}.ini")
        data = _combine_stations(stations, data)
        data = data.replace("$NUMBER_SLOPES", str(number_slopes))
        demfile = get(["domain", "dem", "file"], domain, default="")
        demfile = os.path.splitext(demfile)[0] + ".asc" # auto-generated by the main installer
        data = data.replace("$DEMFILE", demfile)

        demfile_xml = get(["domain", "dem", "file"], domain)
        dem_path, _ = os.path.splitext(demfile_xml)
        epsg = get_epsg_code(f"{dem_path}.prj")
        if not epsg:
            print(r'[E] Could not determine EPSG code needed for raster output. You can e. g. specify it in the file name by putting "epsg#####" at some place.')
            sys.exit()
        data = data.replace("$EPSG", str(epsg))

        os.makedirs(f"./input/{domain}", exist_ok=True)
        with open(f"./input/{domain}/io.ini", "w") as outfile:
            outfile.write(f"# This config file was auto-created by '{__file__}' and can be deleted, but will be re-created.\n")
            outfile.write(data)

def _combine_stations(stations: list, data: str):
    """Combine wind- and snow-stations if they match our common naming convention."""
    station_pairs = ""
    for station in stations:
        if re.match("[a-zA-Z]{3,4}2", station): # snow station: STAT2.smet
            wind_station = station[:-1] + "1"   # wind station: STAT1.smet
            if wind_station in stations:
                station_pairs = station_pairs + station + "::EDIT1 = MERGE\n" + \
                    station + "::ARG1::MERGE = "  + wind_station + "\n" + \
                    station + "::ARG1::PARAMS = VW DW" + "\n" + \
                    station + "::ARG1::MERGE_STRATEGY = STRICT_MERGE" + "\n"
    data = data.replace("$STATION_COMBINATIONS", station_pairs[:-1])
    return data

def _get_env():
    """Get an environment with simlation software binaries in the PATH."""
    mio_env = os.environ.copy()
    mio_env["PATH"] = mio_env["PATH"] + ":" + os.path.expanduser("~snowpack/.local/bin")
    return mio_env

def output_results(domain: str):
    output_dir = os.path.expanduser(f"~snowpack/opera/output/{domain}")
    os.makedirs(output_dir, exist_ok=True)
    profiles = glob.glob(f"{output_dir}/*.pro")

    if exists(["snowpack", "output", "upload"], domain):
        host = get(["snowpack", "output", "upload", "host"], domain)
        output_prog_path = os.path.expanduser("~opera/upload/upload.py")
        for pro in profiles:
            subprocess.call(["python3", output_prog_path, pro, host])
            smet = os.path.join(get_dl_path(domain), Path(pro).stem + ".smet")
            if os.path.exists(smet): # flat field
                subprocess.call(["python3", output_prog_path, smet, host]) # SMET file as downloaded
                smet_processed = os.path.join(output_dir, Path(pro).stem + "_NO_EXP_forcing.smet")
                if not os.path.exists(smet_processed): # SMET file as processed by MeteoIO / SP
                    print(f"[w] SMET file {smet_processed} die not exist when it should have.")
                else:
                    subprocess.call(["python3", output_prog_path, smet_processed, host])

            # if there is an associated wind station upload that one too:
            if re.match("[a-zA-Z]{3,4}2", Path(pro).stem): # snow station: STAT2.smet
                wind_station = Path(pro).stem[:-1] + "1"   # wind station: STAT1.smet
                wind_smet = os.path.join(get_dl_path(domain), wind_station + ".smet")
                if os.path.exists(wind_smet):
                    subprocess.call(["python3", output_prog_path, wind_smet,
                        host + "/" + wind_station + "_wind.smet"])

        web_prog_path = os.path.expanduser("~opera/web/generate_station_list.py")
        subprocess.call(["python3", web_prog_path, host, os.path.join(host, "report.html")])

def run_snowpack(stations: list, sdate: str, edate: str, nr_slopes, domain: str):
    """Run the SNOWPACK model."""
    mio_env = _get_env()
    sim_prog_path = "snowpack"
    dflt_ini = f"./input/{domain}/io.ini"
    print(f"Calling '{sim_prog_path}' with dates {sdate} to {edate}...")

    flavor = get(["snowpack", "mode", "flavor"], domain, default="generic") # the SNOWPACK model configuration type (set by the user)
    preprocessing_flavor = flavor # the data preprocessing type (set programmatically in case the standard fails)

    faulty_stations = []
    for station in stations:
        retry = True
        while retry: # cascade through a number of fallback ini files on lacking data and errors
            _update_ini(stations, station, nr_slopes, flavor, preprocessing_flavor, domain) # compile our SNOWPACK ini
            try:
                # give list of stations again in case there are rogue data files:
                sp_out = subprocess.check_output([sim_prog_path, "-c", dflt_ini, "-b", sdate, "-e", edate,
                    station], env=mio_env)
                sp_out = sp_out.decode("UTF-8").strip()
                print(sp_out)
                if "No valid data for station" in sp_out:
                    if preprocessing_flavor == "fallback": # we have already tried...
                        print("[E] Station lacking data even with generous interpolations.")
                        # update status from "must interpolate more" to "can't do it":
                        for stat in faulty_stations:
                            if stat["stationId"] == station:
                                stat["status"] = 2
                                stat["error"] = sp_out
                                break
                        retry = False # exit the loop and use output up to that date
                    else:
                        print("[w] Station lacking data! Using fallback ini...")
                        preprocessing_flavor = "fallback"
                        faulty_stations.append( {"stationId": station, "status": 3, "error": sp_out} )
                else:
                    retry = False
            except subprocess.CalledProcessError as err:
                sp_out = err.output.decode("UTF-8").strip()
                if not sp_out:
                    sp_out = "Process terminated - check the logs."
                retry = False
                faulty_stations.append( {"stationId": station, "status": 1, "error": sp_out} )

    faulty_stat_file = f"./output/{domain}/faulty_stations.json"
    try:
        os.remove(faulty_stat_file)
    except FileNotFoundError:
        pass
    if faulty_stations:
        with open(faulty_stat_file, "w") as jsonfile:
            json.dump(faulty_stations, jsonfile)

def run_domain(sdate, edate, edate_data=None, domain="default", force_run=False):
    """Download stations data, run SNOWPACK on them and upload the results.

    Arguments:
        sdate (str): Simulation start date in ISO format.
        edate (str): Simulation end date in ISO format.
        domain (str): Name of domain to run.
    """

    if not edate_data:
        edate_data = edate
    stations = getlist(["snowpack", "meteo", "stations"], domain)
    aspects = get(["snowpack", "slope", "aspects"], domain, default="")
    angles = get(["snowpack", "slope", "angles"], domain, default="")
    if "0" in angles.split():
        sys.exit("[E] A slope angle of 0 only makes sense for the flat field where this is implied anyway. Please correct your domain settings.")
    stations_not_updated = _download_sp_stations(stations, sdate, edate_data, domain)
    stations_updated = [ss for ss in stations if ss not in stations_not_updated]

    if force_run:
        stations_updated = stations
    if len(stations_updated) > 0:
        _create_aux_files(aspects, angles, domain) # e. g. sno files
        nr_slopes = len(aspects.split()) + 1 # angled slopes + flat field
        run_snowpack(stations_updated, sdate, edate, nr_slopes, domain)
        output_results(domain)
        subprocess.call(["python3", _map_generation_script])
    else:
        print(f'[i] SNOWPACK has nothing to do for domain "{domain}".')

    print(__file__ + " done")

if __name__ == "__main__":
    if len(sys.argv) > 4:
        print("[E] Synopsis: python3 run_snowpack.py [<domain>] [<start_date> <end_date>]")
        print("[E]       or: python3 run_snowpack.py <domain> <force_run>")
        sys.exit()

    domain = "default"
    if len(sys.argv) >= 2:
        domain = sys.argv[1]
    force_run = False
    edate_data = None
    if len(sys.argv) == 3:
        force_run = bool(sys.argv[2])
    if len(sys.argv) == 4:
        sdate = sys.argv[2]
        edate = sys.argv[3]
    else:
        sdate = get(["snowpack", "mode", "season", "start"], domain)
        edate = get(["snowpack", "mode", "season", "end"], domain)
        if awio.validate_date(sdate, "%m-%d"):
            dt = datetime.strptime(sdate, "%m-%d")
            cyear = datetime.now().year
            cmonth = datetime.now().month
            syear = cyear if cmonth >= dt.month else cyear - 1
            sdate = f"{syear}-{sdate}T00:00:00Z"
        else:
            print("[E] Unable to resolve SNOWPACK start date.")
            exit(1)
        if edate.lower() == "now":
            dt_format = "%Y-%m-%dT%H:%M:%SZ"
            # calc up until some hours ago (the same as for when we download data):
            edate = (datetime.now() - timedelta(hours=_data_is_young)).strftime(dt_format)
            edate_data = datetime.now().strftime(dt_format)
        else:
            print("[E] Unable to resolve SNOWPACK end date.")
            exit(1)

    run_domain(sdate, edate, edate_data, domain=domain, force_run=force_run)
